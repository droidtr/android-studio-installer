#!/bin/bash
if [ "$USER" == "root" ]
then
  echo "Uygulama siliniyor"
  rm -rf /opt/android-studio > /dev/null
  echo "Başlatıcı siliniyor."
  rm -rf  /usr/share/applications/android-studio.desktop
  echo "SDK siliniyor."
  rm -rf  /home/*/.android/
  rm -rf  /home/*/Android/
  echo "Bitti."
else
  echo "Bu işlem için root yetkisi gerekir."
fi
